package com.example.kjankiewicz.android_14w01_mywebservices

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log

import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_my_main.*

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.PropertyInfo
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapPrimitive
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE

import java.lang.ref.WeakReference
import java.util.HashMap


class MyMainActivity : AppCompatActivity() {


    // Before try RESTful Services
    // 1. Install Neo4j on localhost
    // 2. Run it (bin>neo4j console)
    // 3. Set password as neo4j/neo4j

    private var mNeo4jRootURL = "http://10.0.2.2:7474/db/data" //"http://localhost:7474/db/data"; // "http://jankiewicz.pl"; //"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        far2CelButton.setOnClickListener {
            if (far2CelInputEditText.text.isNotEmpty())
                CallBigWSAsyncTask(this).execute(far2CelInputEditText.text.toString())
        }

        // RESTful part

        getRootButton.setOnClickListener { callGetRESTfulWS(mNeo4jRootURL) }

        getLabelsButton.setOnClickListener {
            callGetRESTfulWS("$mNeo4jRootURL/labels")
            consumeJSON(restFullResultTextView.text.toString())
        }

        setLabelsButton.setOnClickListener {
            val cypherJSONObject = JSONObject()
            try {
                cypherJSONObject.put("statement", "MATCH (n) SET n :person RETURN n")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val cypherStatementsJSONObject = JSONArray()
            cypherStatementsJSONObject.put(cypherJSONObject)

            val transactionJSONObject = JSONObject()
            try {
                transactionJSONObject.put("statements", cypherStatementsJSONObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            callPostRESTfulWS("$mNeo4jRootURL/transaction/commit",
                    transactionJSONObject)
        }

        putMeButton.setOnClickListener {
            val meAsJSONObject = JSONObject()
            try {
                meAsJSONObject.put("name", "Krzysztof")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            callPostRESTfulWS("$mNeo4jRootURL/node",
                    meAsJSONObject)
        }


    }

    private class CallBigWSAsyncTask// only retain a weak reference to the activity
    internal constructor(context: MyMainActivity) : AsyncTask<String, Void, String>() {

        private val activityReference: WeakReference<MyMainActivity> = WeakReference(context)


        override fun doInBackground(vararg params: String): String? {
            val activity = activityReference.get()
            if (activity == null || activity.isFinishing) return null

            Log.i(TAG, "doInBackground")

            val request = SoapObject(TNS, OPERATION)
            val celsiusPropertyInfo = PropertyInfo()
            celsiusPropertyInfo.setName("Fahrenheit")
            celsiusPropertyInfo.value = params[0]
            celsiusPropertyInfo.setType(Double::class.javaPrimitiveType)
            request.addProperty(celsiusPropertyInfo)
            val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
            envelope.dotNet = true
            envelope.setOutputSoapObject(request)
            val androidHttpTransport = HttpTransportSE(LOCATION)
            try {
                androidHttpTransport.call(ACTION, envelope)
                val response = envelope.response as SoapPrimitive
                return response.toString()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            val activity = activityReference.get()
            if (activity == null || activity.isFinishing) return

            Log.i(TAG, "onPostExecute")
            var intResult = -999
            if (result != null)
                intResult = Math.round(java.lang.Float.valueOf(result))
            activity.far2CelResultTextView.text = String.format(activity.getString(R.string.result_int_inF), intResult)
        }

        override fun onPreExecute() {
            val activity = activityReference.get()
            if (activity == null || activity.isFinishing) return

            Log.i(TAG, "onPreExecute")
            activity.far2CelResultTextView.text = activity.getString(R.string.wait)
        }

        override fun onProgressUpdate(vararg values: Void) {
            Log.i(TAG, "onProgressUpdate")
        }
        companion object {
            const val TAG = "CallBigWSAsyncTask"
            const val TNS = "https://www.w3schools.com/xml/"
            const val OPERATION = "FahrenheitToCelsius"
            const val LOCATION = "https://www.w3schools.com/xml/tempconvert.asmx"
            const val ACTION = "https://www.w3schools.com/xml/FahrenheitToCelsius"
        }
    }

    private fun callGetRESTfulWS(url: String) {
        val queue = Volley.newRequestQueue(this)

        val stringRequest = object : StringRequest(Request.Method.GET, url,
                { response -> restFullResultTextView.text = response },
                { error ->
                    restFullResultTextView.text = String.format(getString(R.string.not_work), error.message)
                }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val encodedCredentials = Base64.encodeToString("neo4j:neo4j".toByteArray(), Base64.NO_WRAP)
                headers["Authorization"] = "Basic $encodedCredentials"

                return headers
            }
        }
        queue.add(stringRequest)
    }

    private fun callPostRESTfulWS(url: String, jsonBody: JSONObject) {
        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = object : JsonObjectRequest(Request.Method.POST, url,
                jsonBody,
                { response -> restFullResultTextView.text = response.toString() },
                { error ->
                    restFullResultTextView.text = String.format(getString(R.string.not_work), error.message)
                }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val encodedCredentials = Base64.encodeToString("neo4j:neo4j".toByteArray(), Base64.NO_WRAP)
                headers["Authorization"] = "Basic $encodedCredentials"

                return headers
            }
        }

        queue.add(jsonObjectRequest)
    }

    private fun consumeJSON(result: String?) {
        Log.i(TAG, "consumeJSON")
        if (result != null)
            try {
                val tokener = JSONTokener(result)
                while (tokener.more()) {
                    val value = tokener.nextValue()
                    if (value is JSONObject) {
                        if (value.has("node_labels"))
                            restFullResultTextView.text = value.getString("node_labels")
                        else
                            restFullResultTextView.text = result
                    } else
                        restFullResultTextView.text = result
                }
            } catch (e: JSONException) {
                Log.e(TAG, e.message)
                e.printStackTrace()
            }
        else
            restFullResultTextView.text = getString(R.string._null)
    }

    companion object {
        const val TAG: String = "MyMainActivity"
    }

}
